Clone your repository.

Once cloned on your local machine, import it into your Eclipse EE IDE using the following commands

File--> Import --> Existing Maven Projects

and then navigate to the folder where you cloned your git repository. 

IDE will recognize the pom.xml file present in the root of the project and recognizes to import it. Follow the defaults to finish importing

Then run the project on your Tomcat webserver on Eclipse. Your webservice can be accessed at

http://localhost:8080/simple-service-webapp/webapi/myresource

Look into your web.xml file and MyResource.java file to figure out why the path is like this

## Build

You can build from mvn using 'mvn clean install' command

Once it runs successfully you will see 'simple-service-webapp.war' file in the target folder.

Drop this file inside the webapps folder of your Tomcat server and start your Tomcat by navitagig to the 'bin' folder of the Tomcat installation and running

`startup.sh`

This will deploy the web application and you can access the webpage at the same url as before: http://localhost:8080/simple-service-webapp/webapi/myresource

Note:  If your Tomcat is already running at port 8080 then startup.sh script will be unable to deploy the Tomcat at 8080. In which case you have to kill your tomcat server from Eclipse or start this Tomcat at a different port by adding '-port 8081'. Refer the startup.sh script to learn more.